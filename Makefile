# gather source list
SOURCES = $(shell find src/test -name "*.cpp")

# create object and dependency lists (for link)
OBJECTS = $(patsubst src/%.cpp, obj/%.o, $(SOURCES))
DEPENDS = $(patsubst obj/%.o, obj/%.d, $(OBJECTS))

DEFINES = -DQT_CORE_LIB -DQT_GUI_LIB

# compile & link options
# -Wall: all warnings -Wextra: extra warnings -g: debug information
CFLAGS = `pkg-config --cflags QtCore QtGui` $(DEFINES) -Wall -Wextra -g
LFLAGS = `pkg-config --libs QtCore QtGui`

CC = g++

all :
	cd src/test/qt; $(MAKE) $(MAKEFLAGS) all
	cd src/test; $(MAKE) $(MAKEFLAGS) all
	$(MAKE) $(MAKEFLAGS) bin/test

# link executable
bin/test : obj/test.o $(OBJECTS)
	@test -d $(@D) || mkdir -p $(@D)
	$(CC) -o $@ $^ $(LFLAGS)

# compile sources
obj/%.o : src/%.cpp
	@test -d $(@D) || mkdir -p $(@D)
	$(CC) $(CFLAGS) -o $@ -c $<

-include $(DEPENDS)

clean :
	cd src/test/qt; $(MAKE) $(MAKEFLAGS) clean
	cd src/test; $(MAKE) $(MAKEFLAGS) clean
	rm -rf bin obj

# $@ : The target of the current rule.
# $< : The first dependency of the target.
# $^ : All the dependencies of the target.
# $(@D) : The directory part of the target, with the trailing slash removed.
