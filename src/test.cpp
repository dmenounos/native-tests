#include <QtGui/QApplication>
#include "test/qt/FrameWidget.hpp"
#include "test/qt/FrameWindow.hpp"
#include "test/array.hpp"
#include "test/frame.hpp"
#include "test/pack.hpp"
#include "test/property.hpp"
#include "test/types.hpp"
#include "test/valref.hpp"

int main(int argc, char* argv[])
{
	printLimits();

	Array array;
	array.test();

	Pack pack;
	pack.test();

	valref();

	// QApplication app(argc, argv);

	// Property property;
	// property.test();

	// FrameWidget* frameWidget = new FrameWidget();
	// frameWidget->setMouseTracking(true);

	// FrameWindow* frameWindow = new FrameWindow(frameWidget);
	// frameWindow->show();

	// Frame frame;
	// frame.show();

	// return app.exec();

	return 0;
}
