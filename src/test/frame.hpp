#ifndef FRAME_HPP
#define FRAME_HPP

#include <QtGui/QWidget>
#include <QtGui/QPaintEvent>

class Frame : public QWidget {

	Q_OBJECT

public:

	Frame();

protected:

	void paintEvent(QPaintEvent* event);
};

#endif
