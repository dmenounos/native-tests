#include <iostream>
#include "array.hpp"

Array::Array(int cols, int rows)
	: mArray(new int[cols * rows])
	, mCols(cols)
	, mRows(rows) {
	int v = 0;

	for (int r = 0; r < mRows; r++) {
		for (int c = 0; c < mCols; c++) {
			setItemAt(r, c, v++);
		}
	}
}

Array::~Array() {
	delete [] mArray;
}

void Array::test() {
	std::cout << "Array::test()" << std::endl;

	for (int r = 0; r < mRows; r++) {
		for (int c = 0; c < mCols; c++) {
			std:: cout << getItemAt(r, c) << " ";
		}

		std::cout << std::endl;
	}

	std::cout << std::endl;
}

int Array::getItemAt(int row, int col) {
	int index = row * mCols + col;
	return mArray[index];
}

void Array::setItemAt(int row, int col, int val) {
	int index = row * mCols + col;
	mArray[index] = val;
}
