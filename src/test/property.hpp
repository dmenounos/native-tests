#ifndef PROPERTY_HPP
#define PROPERTY_HPP

#include <iostream>
#include <QtCore/QObject>
#include <QtCore/QMetaType>

static int count = 0;

struct Value {

	int id;

	Value() : id(++count) {
		std::cout << "### Value constructor { this: " << this << " }" << std::endl;
	}

	Value(const Value& ref) {
		id = ref.id;
		std::cout << "### Value copy constructor { this: " << this << ", ref: " << &ref << " }" << std::endl;
	}

	Value& operator=(const Value& ref) {
		id = ref.id;
		std::cout << "### Value assignment operator { this: " << this << ", ref: " << &ref << " }" << std::endl;
		return *this;
	}
};

Q_DECLARE_METATYPE(Value);

class Property : public QObject {

	Q_OBJECT

	Q_PROPERTY(Value foo READ foo)
	Q_PROPERTY(Value bar READ bar)

public:

	Property();

	const Value* foo() const;
	const Value& bar() const;

	void test();

private:

	Value m_foo;
	Value m_bar;
};

#endif
