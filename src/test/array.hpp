#ifndef ARRAY_HPP
#define ARRAY_HPP

class Array {

public:

	Array(int cols = 10, int rows = 10);

	~Array();

	void test();

private:

	int getItemAt(int row, int col);

	void setItemAt(int row, int col, int val);

private:

	int* mArray;
	int mCols;
	int mRows;
};

#endif
