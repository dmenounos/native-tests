#ifndef FRAME_HPP
#define FRAME_HPP

#include <QtGui/QWidget>
#include <QtGui/QPaintEvent>
#include <QtGui/QResizeEvent>
#include <QtGui/QMouseEvent>
#include "AspectRatio.hpp"

class FrameWidget : public QWidget
{
	Q_OBJECT

	public:
	FrameWidget(QWidget* parent = 0);

	signals:
	void proportionsChange(QSize size);

	protected:
	void paintEvent(QPaintEvent* event);
	void resizeEvent(QResizeEvent* event);
	void mouseMoveEvent(QMouseEvent* event);

	private:
	void updateViewportProportions();
	void updateWindowProportions();

	public:
	const QSize& refSize() const;
	AspectRatio aspectRatio() const;
	bool scaleMode() const;

	public slots:
	void setRefSize(QSize refSize);
	void setAspectRatio(AspectRatio ratio);
	void setScaleMode(bool mode);

	protected:
	const QRect& viewport() const;
	const QRect& window() const;
	const QPoint& mouse() const;

	private:
	QSize mRefSize;
	AspectRatio mAspectRatio;
	bool mScaleMode;
	QRect mViewport;
	QRect mWindow;
	QPoint mMouse;
};

#endif
