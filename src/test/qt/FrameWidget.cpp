#include <QtGui/QPainter>
#include "FrameWidget.hpp"
#include "common.h"

FrameWidget::FrameWidget(QWidget* parent)
	: QWidget(parent)
	, mRefSize(QSize(800, 480))
	, mAspectRatio(AspectRatio_0)
	, mScaleMode(false)
	, mViewport()
	, mWindow()
	, mMouse()
{
	DEBUG("FrameWidget() -->");

	setMinimumSize(mRefSize.width(), mRefSize.height());

	DEBUG("FrameWidget() <--");
}

void FrameWidget::paintEvent(QPaintEvent*)
{
	QPainter painter(this);
	painter.fillRect(rect(), Qt::black);
	painter.setViewport(viewport());
	painter.setWindow(window());
	painter.fillRect(window(), QBrush(Qt::green));
	painter.drawText(10, 20, QString("ratio: ") + QString::number(AspectValues[aspectRatio()]));
	painter.drawText(10, 40, QString("viewport (x, y, width, height): ") + QString::number(viewport().x()) + ", " + QString::number(viewport().y()) + ", " + QString::number(viewport().width()) + ", " + QString::number(viewport().height()));
	painter.drawText(10, 60, QString("window (x, y, width, height): ") + QString::number(window().x()) + ", " + QString::number(window().y()) + ", " + QString::number(window().width()) + ", " + QString::number(window().height()));
	painter.drawText(10, 80, QString("mouse (x, y): ") + QString::number(mouse().x()) + ", " + QString::number(mouse().y()));
}

void FrameWidget::resizeEvent(QResizeEvent*)
{
	DEBUG("FrameWidget::resizeEvent() -->");

	updateViewportProportions();

	DEBUG("FrameWidget::resizeEvent() <--");
}

void FrameWidget::mouseMoveEvent(QMouseEvent* event)
{
	int x = event->x();
	int y = event->y();

	if (mViewport.contains(x, y)) {
		int horOffset = mViewport.x();
		int verOffset = mViewport.y();

		float horRatio = (float) mWindow.width() / mViewport.width();
		float verRatio = (float) mWindow.height() / mViewport.height();

		mMouse.setX((x - horOffset) * horRatio + 0.5f);
		mMouse.setY((y - verOffset) * verRatio + 0.5f);
	}
}

/**
 * Update physical proportions.
 * Same as widget size, unless a specific aspect ratio is chosen.
 */
void FrameWidget::updateViewportProportions()
{
	DEBUG("FrameWidget::updateViewportProportions() -->");

	mViewport.setRect(0, 0, width(), height());

	if (aspectRatio() != AspectRatio_0) {
		float curRatio = (float) width() / height();
		float fixRatio = AspectValues[aspectRatio()];

		if (curRatio > fixRatio) {
			mViewport.setWidth(mViewport.height() * fixRatio + 0.5f);
			mViewport.moveLeft((width() - mViewport.width()) / 2);
		}
		else if (curRatio < fixRatio) {
			mViewport.setHeight(mViewport.width() / fixRatio + 0.5f);
			mViewport.moveTop((height() - mViewport.height()) / 2);
		}
	}

	updateWindowProportions();

	DEBUG("FrameWidget::updateViewportProportions() <--");
}

/**
 * Update logical proportions.
 * Same as physical, unless scale mode is enabled.
 */
void FrameWidget::updateWindowProportions()
{
	DEBUG("FrameWidget::updateWindowProportions() -->");

	if (mScaleMode) {
		mWindow.setSize(mRefSize);

		float refRatio = (float) mRefSize.width() / mRefSize.height();
		float curRatio = (float) mViewport.width() / mViewport.height();

		if (refRatio > curRatio) {
			mWindow.setWidth(mWindow.height() * curRatio + 0.5f);
		}
		else if (refRatio < curRatio) {
			mWindow.setHeight(mWindow.width() / curRatio + 0.5f);
		}
	}
	else {
		mWindow.setSize(mViewport.size());
	}

	setMinimumSize(mRefSize.width(), mRefSize.height());
	emit proportionsChange(mWindow.size());

	DEBUG("FrameWidget::updateWindowProportions() <--");
}

const QSize& FrameWidget::refSize() const
{
	return mRefSize;
}

void FrameWidget::setRefSize(QSize refSize)
{
	mRefSize = refSize;
	updateWindowProportions();
}

AspectRatio FrameWidget::aspectRatio() const
{
	return mAspectRatio;
}

void FrameWidget::setAspectRatio(AspectRatio ratio)
{
	mAspectRatio = ratio;
	updateViewportProportions();
}

bool FrameWidget::scaleMode() const
{
	return mScaleMode;
}

void FrameWidget::setScaleMode(bool mode)
{
	mScaleMode = mode;
	updateWindowProportions();
}

const QRect& FrameWidget::viewport() const
{
	return mViewport;
}

const QRect& FrameWidget::window() const
{
	return mWindow;
}

const QPoint& FrameWidget::mouse() const
{
	return mMouse;
}
