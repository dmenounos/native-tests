#ifndef ASPECTRATIO_HPP
#define ASPECTRATIO_HPP

enum AspectRatio
{
	AspectRatio_0,
	AspectRatio_4_3,
	AspectRatio_16_9,
	AspectRatio_16_10
};

const float AspectValues[] = {
	0,
	(float) 4 / 3,
	(float) 16 / 9,
	(float) 16 / 10
};

#endif
