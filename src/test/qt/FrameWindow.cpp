#include <QtGui/QActionGroup>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include "FrameWindow.hpp"
#include "common.h"

FrameWindow::FrameWindow(FrameWidget* frameWidget)
	: mFrameWidget(frameWidget)
{
	DEBUG("FrameWindow() -->");

	initActions();
	initMenuBar();
	setCentralWidget(frameWidget);

	DEBUG("FrameWindow() <--");
}

void FrameWindow::initActions()
{
	mQuitAction = new QAction("Quit", this);
	connect(mQuitAction, SIGNAL(triggered()),
	        this, SLOT(close()));

	mScaleModeAction = new QAction("Scale", this);
	mScaleModeAction->setCheckable(true);
	mScaleModeAction->setChecked(false);
	connect(mScaleModeAction, SIGNAL(triggered(bool)),
	        this, SLOT(setScaleMode(bool)));

	m_0_RatioAction = new QAction("Default", this);
	m_0_RatioAction->setCheckable(true);
	m_0_RatioAction->setChecked(true);

	m_4_3_RatioAction = new QAction("4:3", this);
	m_4_3_RatioAction->setCheckable(true);

	m_16_9_RatioAction = new QAction("16:9", this);
	m_16_9_RatioAction->setCheckable(true);

	m_16_10_RatioAction = new QAction("16:10", this);
	m_16_10_RatioAction->setCheckable(true);

	QActionGroup* aspectRatioGroup = new QActionGroup(this);
	aspectRatioGroup->addAction(m_0_RatioAction);
	aspectRatioGroup->addAction(m_4_3_RatioAction);
	aspectRatioGroup->addAction(m_16_9_RatioAction);
	aspectRatioGroup->addAction(m_16_10_RatioAction);
	connect(aspectRatioGroup, SIGNAL(triggered(QAction*)),
	        this, SLOT(setAspectRatio(QAction*)));
}

void FrameWindow::initMenuBar()
{
	QMenu* fileMenu = new QMenu("File", this);
	fileMenu->addAction(mQuitAction);

	QMenu* aspectRatioMenu = new QMenu("Aspect Ratio", this);
	aspectRatioMenu->addAction(m_0_RatioAction);
	aspectRatioMenu->addAction(m_4_3_RatioAction);
	aspectRatioMenu->addAction(m_16_9_RatioAction);
	aspectRatioMenu->addAction(m_16_10_RatioAction);

	QMenu* optionsMenu = new QMenu("Options", this);
	optionsMenu->addAction(mScaleModeAction);
	optionsMenu->addMenu(aspectRatioMenu);

	menuBar()->addMenu(fileMenu);
	menuBar()->addMenu(optionsMenu);
}

void FrameWindow::setScaleMode(bool mode)
{
	mFrameWidget->setScaleMode(mode);
}

void FrameWindow::setAspectRatio(QAction* action)
{
	AspectRatio ratio = AspectRatio_0;

	if (action == m_0_RatioAction)
		ratio = AspectRatio_0;
	else if (action == m_4_3_RatioAction)
		ratio = AspectRatio_4_3;
	else if (action == m_16_9_RatioAction)
		ratio = AspectRatio_16_9;
	else if (action == m_16_10_RatioAction)
		ratio = AspectRatio_16_10;

	mFrameWidget->setAspectRatio(ratio);
}
