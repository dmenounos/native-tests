#ifndef FRAMEWINDOW_HPP
#define FRAMEWINDOW_HPP

#include <QtGui/QMainWindow>
#include <QtGui/QAction>
#include "FrameWidget.hpp"

class FrameWindow : public QMainWindow
{
	Q_OBJECT

	public:
	FrameWindow(FrameWidget* frameWidget);

	private:
	void initActions();
	void initMenuBar();

	private slots:
	void setScaleMode(bool mode);
	void setAspectRatio(QAction* action);

	private:
	FrameWidget* mFrameWidget;
	QAction* mQuitAction;
	QAction* mScaleModeAction;
	QAction* m_0_RatioAction;
	QAction* m_4_3_RatioAction;
	QAction* m_16_9_RatioAction;
	QAction* m_16_10_RatioAction;
};

#endif
