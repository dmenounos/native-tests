#ifndef COMMON_H
#define COMMON_H

#define DEBUG(...) \
	fprintf(stderr, __VA_ARGS__); \
	fprintf(stderr, "\n");

#include <stdlib.h>
#include <stdio.h>

#include <stdexcept>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

#endif
