#include <stdlib.h> // EXIT_SUCCESS
#include <stdio.h>  // printf
#include <limits.h> // bool, char, short, int, long limits
#include <float.h>  // float, double limits

#include "types.hpp"

void printLimits() {
	printf( "type\tsize\tmin\t\t\tmax\n" );
	printf( "------------------------------------------------------------\n" );
	printf( "bool\t%zu\n",                sizeof(bool)                       );
	printf( "char\t%zu\t%d\t\t\t%d\n",    sizeof(char),   CHAR_MIN, CHAR_MAX );
	printf( "short\t%zu\t%hd\t\t\t%hd\n", sizeof(short),  SHRT_MIN, SHRT_MAX );
	printf( "int\t%zu\t%d\t\t%d\n",       sizeof(int),    INT_MIN,  INT_MAX  );
	printf( "long\t%zu\t%ld\t%ld\n",      sizeof(long),   LONG_MIN, LONG_MAX );
	printf( "float\t%zu\t%e\t\t%e\n",     sizeof(float),  FLT_MIN,  FLT_MAX  );
	printf( "double\t%zu\t%e\t\t%e\n",    sizeof(double), DBL_MIN,  DBL_MAX  );
}

/*
int main()
{
	printLimits();
	return EXIT_SUCCESS;
}
*/
