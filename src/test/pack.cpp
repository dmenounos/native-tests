#include <iostream>
#include "pack.hpp"

Pack::Pack() {
	mData = new int[2];
	mData[0] = pack(1, 1);
	mData[1] = pack(127, 127);
	mData[2] = pack(255, 255);
}

Pack::~Pack() {
	delete [] mData;
}

void Pack::test() {
	std::cout << "Pack::test()" << std::endl;

	int a, b;

	for (int x = 0; x < 3; x++) {
		unpack(mData[x], a, b);

		std::cout << x << ": { a: " << a << ", b: " << b << " }" << std::endl;

		std::cout << "Packed word: ";
		printBinary(mData[x]);

		std::cout << "Un-packed a: ";
		printBinary(a);

		std::cout << "Un-packed b: ";
		printBinary(b);
	}

	std::cout << std::endl;
}

int Pack::pack(int a, int b) {
	int v = a & 0xFF;
	v = (v << 8) | (b & 0xFF);
	return v;
}

void Pack::unpack(int v, int& a, int& b) {
	b = v & 0xFF;
	a = (v >> 8) & 0xFF;
}

void Pack::printBinary(int n) {
	for (int i = 31; i >= 0; i--) {
		int bit = ((n >> i) & 1);
		std::cout << bit;
	}

	std::cout << std::endl;
}
