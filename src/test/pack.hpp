#ifndef PACK_HPP
#define PACK_HPP

class Pack {

public:

	Pack();

	~Pack();

	void test();

private:

	int pack(int a, int b);

	void unpack(int v, int& a, int& b);

	void printBinary(int n);

	int* mData;
};

#endif
