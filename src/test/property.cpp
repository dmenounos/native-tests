#include <QtCore/QVariant>
#include "property.hpp"

Property::Property()
	: m_foo()
	, m_bar() {
}

const Value* Property::foo() const {
	return &m_foo;
}

const Value& Property::bar() const {
	return m_bar;
}

void Property::test() {
	std::cout << "Property::test()" << std::endl;

	std::cout << "Accessing foo property dynamicaly via QVariant" << std::endl;
	QVariant vf = this->property("foo");
	Value f = vf.value<Value>();

	std::cout << "Accessing bar property dynamicaly via QVariant" << std::endl;
	QVariant vb = this->property("bar");
	Value b = vb.value<Value>();

	std::cout << "  values:" << std::endl;
	std::cout << "  foo: " << this->m_foo.id << ", f: " << f.id << std::endl;
	std::cout << "  bar: " << this->m_bar.id << ", b: " << b.id << std::endl;

	std::cout << "  addressses:" << std::endl;
	std::cout << "  foo: " << &this->m_foo << ", " <<  this->foo() << ", f: " << &f << std::endl;
	std::cout << "  bar: " << &this->m_bar << ", " << &this->bar() << ", b: " << &b << std::endl;
	std::cout << std::endl;
}
