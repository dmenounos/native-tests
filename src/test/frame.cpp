#include <QtGui/QPainter>
#include "frame.hpp"

Frame::Frame() {
	int size = 500;
	setFixedSize(size, size);
}

void Frame::paintEvent(QPaintEvent*) {
	int size = 300;
	QRect vp((width() - size) / 2, (height() - size) / 2, size, size);
	QRect win(0, 0, size, size);

	QPainter painter(this);
	painter.fillRect(rect(), QBrush(Qt::black));
	painter.setViewport(vp);
	painter.setWindow(win);
	painter.setClipRect(win);
	painter.fillRect(win, QBrush(Qt::green));

	painter.fillRect(-50, -50, 150, 150, QBrush(Qt::blue));
	painter.fillRect(200, -50, 150, 150, QBrush(Qt::red));
	painter.fillRect(200, 200, 150, 150, QBrush(Qt::blue));
	painter.fillRect(-50, 200, 150, 150, QBrush(Qt::red));
	painter.fillRect(100, 100, 100, 100, QBrush(Qt::magenta));
}
