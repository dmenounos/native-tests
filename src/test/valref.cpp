#include <iostream>

#include "valref.hpp"

using namespace std;


Bar::Bar() {
	cout << "Bar constructor { this: " << this << " }" << endl;
}

Bar::Bar(const Bar& ref) {
	cout << "Bar copy constructor { this: " << this << ", ref: " << &ref << " }" << endl;
}

Bar& Bar::operator=(const Bar& ref) {
	cout << "Bar assignment operator { this: " << this << ", ref: " << &ref << " }" << endl;
	return *this;
}

Bar::~Bar() {
	cout << "Bar destructor { this: " << this << " }" << endl;
}


Foo::Foo(Bar bar, char dummy) :
	mBar(bar) { // Bar copy constructor - no optimization
	cout << "Foo constructor #1 by value " << dummy << endl;
}

Foo::Foo(const Bar& bar, int dummy) :
	mBar(bar) { // Bar copy constructor - no optimization
	cout << "Foo constructor #2 by reference - inlist " << dummy << endl;
}

Foo::Foo(const Bar& bar, long dummy) {
	mBar = bar; // Bar assignment operator - no optimization
	cout << "Foo constructor #3 by reference - assign " << dummy << endl;
}

void Foo::doSomething() {
}


Bar b() {
	return Bar();
}


int valref() {
	cout << "Pass By Value" << endl;
	cout << "-------------" << endl;
	Foo(Bar(), '0').doSomething();
	cout << endl;

	cout << "Pass By Reference (inlist)" << endl;
	cout << "--------------------------" << endl;
	Foo(Bar(), 0).doSomething();
	cout << endl;

	cout << "Pass By Reference (assign)" << endl;
	cout << "--------------------------" << endl;
	Foo(Bar(), 0L).doSomething();
	cout << endl;

	cout << "Return By Value" << endl;
	cout << "---------------" << endl;
	Bar bar = b(); // optimization - no copy constructor
	cout << endl;

	return 0;
}
