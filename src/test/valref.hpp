class Bar {

public:

	Bar();

	Bar(const Bar& ref);

	Bar& operator=(const Bar& ref);

	~Bar();
};

class Foo {

public:

	Foo(Bar bar, char dummy);

	Foo(const Bar& bar, int dummy);

	Foo(const Bar& bar, long dummy);

	void doSomething();

private:

	Bar mBar;
};

Bar b();

int valref();
