/*
 * Copyright (c) 2005, Jon �yvind Kjellman <jonkje@softhome.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that redistributions of source
 * code retains the above copyright notice and the following disclaimer.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */

#include <GL/glut.h>
#include <GL/gl.h>
#include <libvisual/libvisual.h>
#include <string.h>
#include <stdlib.h>

VisInput* input = NULL;
VisActor* actor = NULL;
VisVideo* video = NULL;
VisBin* bin = NULL;
VisVideoDepth depth = VISUAL_VIDEO_DEPTH_ERROR;
GLuint window = 0, width = 640, height = 480;

/* Standard glut keyboard function.
 * Kills the program on any keypress. */
void key_func(unsigned char k, int x, int y)
{
  glutDestroyWindow(window);
  exit(0);
}


/* This function is used to set up a sensible 2D environment. */
void setup_2d()
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, width, 0, height, 0.1, 1);
  /* Because (0,0) is in the lower-left corner in OpenGL, but in
   * the top-left corner in Libvisual we need to flip the image.
   */
  glPixelZoom(1, -1);
  glRasterPos3f(0, height  - 1, -0.3);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}


/* Standard glut resize function. */
void resize(int w, int h)
{
  width = w;
  height = h;
  glViewport(0, 0, width, height);

  /* Here we give video a new size. */
  if(visual_video_set_dimension(video, width, height) != VISUAL_OK)
    visual_log(VISUAL_LOG_ERROR, "Unable to resize video.");

  if(depth != VISUAL_VIDEO_DEPTH_GL)
    /* visual_video_set_dimension doesn't resize the buffer so we
     * need to call visual_video_allocate_buffer() This will only 
     * work if you made the buffer either using the same function
     * or (like in this example) by visual_video_new_with_buffer().
     */
    if(visual_video_allocate_buffer(video) != VISUAL_OK)
      visual_log(VISUAL_LOG_ERROR, "Unable to resize buffer.");

  /* This lets actor know about the format change. 
   * Note: by setting rundepth to 0 and forced to FALSE we keep the
   * current rundepth.
   */
  if(visual_actor_video_negotiate(actor, 0, FALSE, FALSE) != VISUAL_OK)
    visual_log(VISUAL_LOG_ERROR, "actor/video negotiation failed.");

  /* OpenGL actors will handle their own OpenGL environment, we however
   * need to ensure a sensible one for drawing the buffer.
   */
  if(depth != VISUAL_VIDEO_DEPTH_GL)
    setup_2d();
}

/* Standard glut display function. */
void display()
{
  glClear(GL_COLOR_BUFFER_BIT);

  /* This renders, to video's buffer or to the screen using OpenGL. */
  visual_bin_run(bin);

  if(depth != VISUAL_VIDEO_DEPTH_GL) {
    /* We have to render video's buffer. */
    GLenum format = GL_R3_G3_B2; /* Reasonable safety against reads outside of the buffer. */
    switch(visual_video_bpp_from_depth(depth)) {
    case 4:
      format = GL_RGBA;
      break;
    case 3:
      format = GL_RGB;
      break;
    case 2:
      format = GL_RGBA4;
      break;
      /*    case 1: defaults. Should not happen, ever. */

    }

    /* Draws the buffer to screen. */
    glDrawPixels(width, height, format,
		 GL_UNSIGNED_BYTE, (void*)video->buffer);
  }

  glutSwapBuffers();
  glutPostRedisplay();
}

int main(int argc, char** argv)
{
  /* GLUT initialization */
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutInitWindowPosition(100, 100);
  glutInitWindowSize(width, height);
  window = glutCreateWindow("LibVisual example");

  /* Sets up GLUT callbacks */
  glutDisplayFunc(display);
  glutReshapeFunc(resize);
  glutKeyboardFunc(key_func);

  /* We init libvisual after glut since some plug-ins need 
   * OpenGL during initialization.
   */
  if(visual_init(&argc, &argv) < 0)
    visual_log(VISUAL_LOG_ERROR, "Couldn't initialize Libvisual.");

  char* input_name = "alsa"; /* Default actor. */
  char* actor_name = "oinksie"; /* Default input. */

  /* We parse the arguments. */
  for(int i = 1; i < argc; ++i) {
    if(strcmp(argv[i], "--actor") == 0 && i+1 < argc)
      actor_name = argv[++i];
    else if(strcmp(argv[i], "--input") == 0 && i+1 < argc)
      input_name = argv[++i];
    else if(strcmp(argv[i], "--help") == 0) {
      printf("LibVisual example.\n");
      printf("Use --actor and --input flags to specify\nplug-in and source from list below.\n");

      /* Here we print out the avaiable input plug-ins. */
      printf("\nAvaiable inputs:\n");
      for(VisListEntry* i = visual_input_get_list()->head; i; i = i->next)
	printf("%s\n", ((VisPluginRef*)i->data)->info->plugname);
      printf("\nAvaiable actors:\n");

      /* Here we print out the avaiable actors. These produce som warnings
       * when compiling, but they should be safe to ignore.
       */
      for(char* name = visual_actor_get_next_by_name_nogl(NULL); name;
	  name = visual_actor_get_next_by_name_nogl(name))
	printf("%s\n", name);
      for(char* name = visual_actor_get_next_by_name_gl(NULL); name;
	  name = visual_actor_get_next_by_name_gl(name))
	  printf("%s (OpenGL)\n", name);
      return 0;
    }
  }

  /* if(!actor_name) actor_name = "oinksie"; */ /* Default actor */
  /* if(!input_name) input_name = "alsa"; */ /* Default input */

  /* This creates a new input, and loads it if the name is valid.
   * Note: it will not return NULL if the name is invalid, however
   * it will fail later at realization.
   */
  if((input = visual_input_new(input_name)) == NULL)
    visual_log(VISUAL_LOG_ERROR, "No input loaded.");
  /* Initializes input plug-in and fails if none was actually loaded. */
  if(visual_input_realize(input) != VISUAL_OK)
    visual_log(VISUAL_LOG_ERROR, "Unable to realize input; %s.", input_name);

  /* Basically the same as with the previus section. */
  if((actor = visual_actor_new(actor_name)) == NULL)
    visual_log(VISUAL_LOG_ERROR, "No actor loaded.");
  if(visual_actor_realize(actor) != VISUAL_OK)
    visual_log(VISUAL_LOG_ERROR, "Unable to realize actor; %s.", actor_name);

  /* Since we're using OpenGL to render the buffer, it is simpler for us
   * if an actor using 8 bit palette renders to video using is 24 bit. This
   * flag will keep track of this situation.
   */
  int pal_to_24 = 0;
  /* This gets all supported depths. */
  depth = visual_actor_get_supported_depth(actor);
  /* This checks for spurious situations we're not able to handle. */
  if(depth == VISUAL_VIDEO_DEPTH_NONE || depth == VISUAL_VIDEO_DEPTH_ERROR)
    visual_log(VISUAL_LOG_ERROR, "Received fubar depthflag.");
  /* The actor either renders using OpenGL or we select the highest possible depth. */
  if(!(depth & VISUAL_VIDEO_DEPTH_GL)) {
    if(depth & VISUAL_VIDEO_DEPTH_32BIT)
      depth = VISUAL_VIDEO_DEPTH_32BIT;
    else if(depth & VISUAL_VIDEO_DEPTH_24BIT)
      depth = VISUAL_VIDEO_DEPTH_24BIT;
    else if(depth & VISUAL_VIDEO_DEPTH_16BIT)
      depth = VISUAL_VIDEO_DEPTH_16BIT;
    else if(depth & VISUAL_VIDEO_DEPTH_8BIT) {
      /* We don't want to deal with the pallett directly so we set 24 bit depth
       * and flags that we wan't video to convert for us. 
       */
      depth = VISUAL_VIDEO_DEPTH_24BIT;
      pal_to_24 = 1;
    }
    /* This just prints some info on the console. */
    visual_log(VISUAL_LOG_INFO, "Using %d bit framebuffer.", visual_video_bpp_from_depth(depth) * 8);
  } else {
    /* The actor will be rendering using OpenGL. */
    depth = VISUAL_VIDEO_DEPTH_GL;
    /*    use_gl = 1;*/
    visual_log(VISUAL_LOG_INFO, "Using OpenGL plug-in.");
  }


  if(depth != VISUAL_VIDEO_DEPTH_GL) {
    /* Plug-in isn't OpenGL. we'll create a video instance with a buffer. */
    video = visual_video_new_with_buffer(width, height, depth);
    /* video needs to know if the actor uses a palette. So that even though the
     * actor renders 8 bit to the video buffer, video exposes a 24 bit buffer
     * to us.
     */
    if(pal_to_24)
      if(visual_video_set_palette(video, visual_actor_get_palette(actor)) != VISUAL_OK)
	visual_log(VISUAL_LOG_ERROR, "Unable to set palette on video.");
  } else {
    /* Plug-in is OpenGL. We don't need a buffer. */
    video = visual_video_new();
    /* We need to inform video of the depth. */
    if(visual_video_set_depth(video, depth) != VISUAL_OK)
      visual_log(VISUAL_LOG_ERROR, "Unable to set depth on video.");
  }

  /* This binds video to actor, meaning actor will render to video. */
  if(visual_actor_set_video(actor, video) != VISUAL_OK)
    visual_log(VISUAL_LOG_ERROR, "Unable to set video for actor.");

  /* This lets actor know about the format of video.
   * The pal_to_24 checks forces video to draw 24 bit to the buffer
   * even though the actor is 8 bit.
   */
  if(visual_actor_video_negotiate(actor,
				  pal_to_24 ? VISUAL_VIDEO_DEPTH_24BIT : 0,
				  FALSE,
				  pal_to_24) != VISUAL_OK)
    visual_log(VISUAL_LOG_ERROR, "actor/video negotiation failed.");

  /* This section will set up a complete pipeline/bin with input, actor
   * and video. This simplifies rendering as we only have to call run on
   * bin.
   */
  if((bin = visual_bin_new()) == NULL)
    visual_log(VISUAL_LOG_ERROR, "Unable to create bin.");
  if(visual_bin_connect(bin, actor, input) != VISUAL_OK)
    visual_log(VISUAL_LOG_ERROR, "Unable to connect bin-actor-input.");
  if(visual_bin_realize(bin) != VISUAL_OK)
    visual_log(VISUAL_LOG_ERROR, "Unable to realize bin.");


  glViewport(0, 0, width, height);

  /* OpenGL actors will handle their own OpenGL environment, we however
   * need to ensure a sensible one for drawing the buffer.
   */
  if(depth != VISUAL_VIDEO_DEPTH_GL)
    setup_2d();

  /* Enters the main (Free)GLUT processing loop. Point of no return. */
  glutMainLoop();

  return 0;
}
