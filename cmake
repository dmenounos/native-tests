#!/bin/sh

do_clean() {
	make -C "src/test/qt" clean
	make -C "src/test" clean
	rm -rf build
}

do_cmake() {
	make -C "src/test/qt"
	make -C "src/test"
	mkdir -p build
	cd build
	cmake ..
	cd ..
}

do_build() {
	cd build
	make
}

case $1 in
	clean)
		do_clean
		;;

	cmake)
		do_cmake
		;;

	build)
		do_build
		;;

	all)
		do_clean
		do_cmake
		do_build
		;;

	*)
		echo "Usage: $0 { clean | cmake | build | all }"
		exit 1
		;;
esac
